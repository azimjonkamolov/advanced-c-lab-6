/*
	Name: Lab 6 problem 2
	Author: Azimjon Kamolov
	Contact: azimjon.6561@gmail.com
	Date: 28/10/18 11:04 P.M
	Description: to use dynamic and find the max among the given values
*/
#include<stdio.h>
#include<stdlib.h>

int main()
{

	int i, n;
	float *p;
	scanf("%d", &n);
	p=(float*)calloc(n,sizeof(float));
	for(i=0;i<n;i++)
	{
		scanf("%f", (p+i));
	}
	
	for(i=1;i<n;++i)
	{
		if(*p<*(p+i))
		{
			*p=*(p+i);
		}
	}
	printf("%.2f", *p);
	free(p);
	return 0;
}