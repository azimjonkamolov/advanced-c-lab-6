/*
	Name: Lab 6 problem 2
	Author: Azimjon Kamolov
	Contact: azimjon.6561@gmail.com
	Date: 28/10/18 11:08 P.M
	Description: matrix string 2 d array with dynamic memory
*/
#include <stdio.h> 
#include <stdlib.h> 
  
int main() 
{ 
    int a, b;
    int raq;
	scanf("%d %d", &b, &a); 
    int *arr = (int *)malloc(a * b * sizeof(int)); 
  
    int i, j, count = 0; 
    for (i = 0; i <  a; i++)
    {
        for (j = 0; j < b; j++)
		{
		 *(arr + i*b + j) = ++count;	
		} 
	}

	raq=97;
    for (i = 0; i <  a; i++)
	{
	  	for (j = 0; j < b; j++)
	  	{
	  		if(raq>=97 && raq <= 122)
	  		{
	  			printf("%c ", raq++);
				if(raq==123)
				{
				raq-=58;	
				}	
			}
			else if(raq>=65 && raq<=90)
			{
				printf("%c ", raq++);
				if(raq==91)
				{
					raq+=6;
				}
			}
		} 
		printf("\n");	
	} 
     
  
   /* Code for further processing and free the  
      dynamically allocated memory */
    
   return 0; 
} 