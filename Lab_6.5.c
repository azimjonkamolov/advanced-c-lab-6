/*
	Name: Lab 6 problem 5
	Author: Azimjon Kamolov
	Contact: azimjon.6561@gmail.com
	Date: 28/10/18 11:08 P.M
	Description: dynamic memory, structures, related to students' info
*/
#include<stdio.h>
#include<stdlib.h>

struct S
{
	char name[10];
	int sub[3];
	float avg;
};

int main()
{
	struct S *p;
	int i, n, sum, j, fol=0;
	scanf("%d", &n);
	p=(struct S*)malloc(n*sizeof(struct S));
	for(i=0;i<n;i++)
	{
		scanf("%s", &(p+i)->name);
		for(j=0;j<3;j++)
		{
			scanf("%d", &(p+i)->sub[j]);
		}
	}
	
	for(i=0;i<n;i++)
	{
		sum=0;
		printf("%s", (p+i)->name);
		for(j=0;j<3;j++)
		{
			sum+=(p+i)->sub[j];
		}
		(p+i)->avg=(float)sum/3;
		
		for(j=0;j<3;j++)
		{
			if((p+i)->sub[j]>89)
			{
				fol+=1;
			}
			else
			{
				fol-=1;
			}	
		}
		
		printf(" ");
		printf("%.1f", (p+i)->avg);
		printf(" ");
		if(fol==3)
		{
			printf("GREAT\n");
		}
		else if(fol==-3)
		{
			printf("BAD\n");
		}
		else
		{
			printf("GREAT BAD\n");
		}
		fol=0;
 	}

	return 0;
}

