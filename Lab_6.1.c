/*
	Name: Lab 6 problem 1
	Author: Azimjon Kamolov
	Contact: azimjon.6561@gmail.com
	Date: 28/10/18 11.05 P.M (KST)
	Description: to use dynamic array to store the values and get the sum
*/
#include<stdio.h>
#include<stdlib.h>

int main()
{

	int i, n, sum=0, *p;	
	scanf("%d", &n);
	p=(int*)malloc(n*sizeof(int));
	for(i=0;i<n;i++)
	{
		scanf("%d", p+i);
		sum+=*(p+i);
	}
	printf("%d", sum);
	free(p);
	return 0;
}