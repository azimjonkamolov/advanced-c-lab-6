/*
	Name: Lab 6 problem 6
	Author: Azimjon Kamolov
	Contact: azimjon.6561@gmail.com
	Date: 28/10/18 11:012 P.M
	Description: lexicographic order, malloc
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct N
{
	char name[30];
};

int main()
{
	struct N *p, *p1, temp[20];
	int i, j, n;
	scanf("%d", &n);
//	=(char*)malloc(n*sizeof(char));
	p=(struct N*)malloc(n*sizeof(struct N));
	
	for(i=0;i<n;++i)
	{
		scanf("%s", &(p+i)->name);
	}

	for(i=0; i<n-1; ++i)
    	for(j=i+1; j<n ; ++j)
        {
            if(*(p+i)->name>*(p+j)->name)
            {
                *temp=*(p+i);
                *(p+i)=*(p+j);
                *(p+j)=*temp;
            }
        }
		for(i=0;i<n;i++)
	{
		printf("%s\n", (p+i)->name);
	}
    
    return 0;
}