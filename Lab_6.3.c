/*
	Name: Lab 6 problem 3
	Author: Azimjon Kamolov
	Contact: azimjon.6561@gmail.com
	Date: 28/10/18 11:07 P.M
	Description: to use dynamic and get student id and delate and check
*/
#include <stdio.h> 
#include <stdlib.h> 

int main() 
{
	int *p;
    int i, n, m;
	scanf("%d", &n);	
    p=(int*)malloc(n*sizeof(int)); 
    for(i = 1; i <=n; i++) 
    	scanf("%d", (p+i));
//    for(i = 1; i <=n; i++) 
//        printf("%d ", *(p + i));
	scanf("%d", &m);
	if(m<n)
	{
		for(i = 1; i <=n-m; i++) 
        	printf("%d\n", *(p + i));	
	}
	free(p);
    return 0; 
} 